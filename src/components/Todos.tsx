
import React, { Fragment, useEffect } from 'react';
import { useStoreState, useStoreActions } from '../hooks';
import TodoItem from './TodoItem';

export default function TodoList() {
    const fetchTodos = useStoreActions(actions => actions.fetchTodos);

    useEffect(() => {
        fetchTodos()
        // eslint-disable-next-line 
    }, [fetchTodos])

    let todos = useStoreState(state => state.todos);

    return (
        <Fragment>
            <h2>Todos</h2>
            <ul>
                {todos.map(todo => <TodoItem todo={todo} key={todo.id} />)}
            </ul>
        </Fragment>
    )
}
