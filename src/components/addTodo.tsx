import React, { useState } from 'react';
import { useStoreActions, useStoreState } from '../hooks';

export default function AddTodo() {
    const [title, setTitle] = useState('');
    const add = useStoreActions(actions => actions.add);
    const todos = useStoreState(state => state.todos);
    return (
        <>
            <form onSubmit={(e) => {
                e.preventDefault();
                add({
                    id: todos.length + 1,
                    title,
                    completed: false
                })
                setTitle('')
            }}>
                <input type="text" value={title} onChange={e => setTitle(e.target.value)} placeholder="add todo title..." />
                <input type="submit" value="Add Todo" />
            </form>
        </>
    )
}