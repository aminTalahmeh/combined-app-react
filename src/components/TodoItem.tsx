import React from 'react';
import { useStoreActions } from '../hooks';

const TodoItem = ({ todo }: any) => {

    const toggle = useStoreActions(actions => actions.toggle);
    const remove = useStoreActions(actions => actions.remove);

    return (
        <div
            className='todo'
            style={{ textDecoration: todo.completed ? 'line-through' : 'none' }}
        >
            <span onClick={() => toggle(todo.id)} style={{ cursor: 'pointer' }}>{todo.title}</span>
            <button onClick={() => remove(todo.id)}>
                <i className="fas fa-trash-alt"></i>
            </button>
        </div>
    )
}

export default TodoItem;