import React from 'react';
import { StoreProvider, createStore } from 'easy-peasy';
import TodosModel from './model';
import Todos from './components/Todos';
import AddTodo from './components/addTodo';
import './App.css';

const store = createStore(TodosModel);

function App() {
  return (
    <StoreProvider store={store}>
      <div className="App">
        <Todos />
        <AddTodo />
      </div>
    </StoreProvider>
  );
}

export default App;
