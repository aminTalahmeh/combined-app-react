import { Action, action, thunk, Thunk } from 'easy-peasy';

export interface TodosModel {
    todos: Array<{ id: number; title: string; completed: boolean }>,
    fetchTodos: Thunk<TodosModel>,
    setTodos: Action<TodosModel, Array<{ id: number; title: string; completed: boolean }>>,
    add: Action<TodosModel, { id: number; title: string; completed: boolean }>,
    toggle: Action<TodosModel, number>,
    remove: Action<TodosModel, number>
}

const todosModel: TodosModel = {
    // todos: [
    //     {
    //         id: 1,
    //         title: 'Take out trash',
    //         completed: true
    //     },
    //     {
    //         id: 2,
    //         title: 'Pickup kids from school',
    //         completed: false
    //     },
    //     {
    //         id: 3,
    //         title: 'Dinner with boss',
    //         completed: false
    //     }
    // ],

    todos: [],

    // Thunks

    fetchTodos: thunk(async actions => {
        const res = await fetch("https://jsonplaceholder.typicode.com/todos?_limit=5")
        const todos = await res.json();
        actions.setTodos(todos);
    }),

    // Actions

    setTodos: action((state, todos) => {
        state.todos = todos;
    }),

    add: action((state, todo) => {
        state.todos = [...state.todos, todo];
    }),

    toggle: action((state, id) => {
        state.todos.map(todo => todo.id === id ? todo.completed = !todo.completed : todo.completed)
    }),

    remove: action((state, id) => {
        state.todos = state.todos.filter(todo => todo.id !== id)
    })
}


export default todosModel 